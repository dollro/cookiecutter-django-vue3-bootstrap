#!/bin/bash
trap "kill 0" EXIT

usage() { 
	echo "Usage: $0 [-c] [-r <django|vue|all>] [-e <path to file>]" 1>&2;
  echo "-r: select which dvmnt servers to start";
	echo "-e: path to file with environment variables to source";
	exit 1; 
	}

while getopts ":e:r:" opt; do
  case $opt in
    e)
			envfile="$OPTARG"
    	;;
    r)
			run="$OPTARG"
			(( $run == "django" || $run == "vue" || $run == "all" )) || usage
			;;
    *)
			usage
			;;
  esac
done
[ -z $envfile ] && [ -z $run ] && usage

if [ ! -z $envfile ]; then
	echo "sourcing environment variables from $envfile"
	set -a
	source $envfile
	set +a
fi

if [ "$run" = "django" ] || [ "$run" = "all" ] ; then

		touch gulp.log
		echo "------ START GULP SESSION ------" >> gulp.log
		npm run dev >> gulp.log 2>&1 &
		PID_GULP=$!
		echo "GULP STARTED (PID: $PID_GULP) ---> Visit website at http://127.0.0.1:3000"
		echo ""

		touch mailhog.log
		echo "------ START GULP SESSION ------" >> mailhog.log
		docker run -p 1025:1025 -p 8025:8025 mailhog/mailhog >> mailhog.log 2>&1 &
		PID_MAILHOG=$!
		echo "DOCKER MAILHOG STARTED (PID: $PID_MAILHOG)"

		if [ "$run" = "all" ]; then 
			npm --prefix ./vue_frontend run serve 2>&1 &
			PID_VUE=$!
			echo "VUE FRONTEND STARTED (PID: $PID_VUE)"
		fi

		#Start local django development server, and wait until it is manually stopped
		python manage.py runserver_plus --cert-file cert/cert.pem --key-file cert/key.pem 127.0.0.1:8000 --nothreading
		#python manage.py runserver 127.0.0.1:8000 --nothreading
		
		echo ""
		echo ""
		echo "Django development server and gulp, mailhog, npm (vue) subprocesses stopped"

fi

if [ "$run" = "vue" ]; then
	npm --prefix ./vue_frontend run serve
fi


