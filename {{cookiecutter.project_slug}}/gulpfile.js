////////////////////////////////
// Setup
////////////////////////////////

// Gulp and package
const { src, dest, parallel, series, watch } = require('gulp')
const pjson = require('./package.json')

// Plugins
const autoprefixer = require('autoprefixer')
const browserSync = require('browser-sync').create()
const concat = require('gulp-concat')
const cssnano = require ('cssnano')
const imagemin = require('gulp-imagemin')
const gulpif = require('gulp-if')
const pixrem = require('pixrem')
const plumber = require('gulp-plumber')
const postcss = require('gulp-postcss')
const reload = browserSync.reload
const rename = require('gulp-rename')
const sass = require('gulp-sass')(require('sass'));
//const sass = require('gulp-sass')
const spawn = require('child_process').spawn
//const uglify = require('gulp-uglify-es').default
const terser = require('gulp-terser');
const sourcemaps = require('gulp-sourcemaps');

// Relative paths function
function pathsConfig() {
  this.app = `./${pjson.name}`
  const vendorsRoot = 'node_modules'

  return {
    cssSource:
    [
      `${this.app}/static/src/sass`
      //`${vendorsRoot}/bootstrap/scss`,
      //`${vendorsRoot}/bootstrap-icons/font`,
    ],
    sass: `${this.app}/static/src/sass`,
    vendorsJs: [
      //`${vendorsRoot}/jquery/dist/jquery.js`,
      //`${vendorsRoot}/@popperjs/core/dist/umd/popper.js`,
      //`${vendorsRoot}/bootstrap/dist/js/bootstrap.bundle.js`,
    ],
    projectJs: `${this.app}/static/src/js/*.js`,
    fontsSource: [
      `${this.app}/static/src/fonts/*`,
      //`${vendorsRoot}/bootstrap-icons/font/fonts/*`,
    ],
    app: this.app,
    templates: `${this.app}/templates`,
    css: `${this.app}/static/css`,
    fonts: `${this.app}/static/fonts`,
    images: `${this.app}/static/images`,
    js: `${this.app}/static/js`,
  }
}

var paths = pathsConfig()

// Get correct proxy domain, depending if it is started locally or via docker
var proxy_target = process.env.PROXY_TARGET;
if (typeof proxy_target == 'undefined') {
  var proxy_target = 'localhost:8000';
}

////////////////////////////////
// Tasks
////////////////////////////////

// Styles autoprefixing and minification
function styles() {
  var processCss = [
      autoprefixer(), // adds vendor prefixes
      pixrem(),       // add fallbacks for rem units
  ]

  var minifyCss = [
      cssnano({ preset: 'default' })   // minify result
  ]

  return src(`${paths.sass}/project.scss`)
    .pipe(sass({
      includePaths: paths.cssSource
    }).on('error', sass.logError))
    .pipe(plumber()) // Checks for errors
    .pipe(postcss(processCss))
    .pipe(dest(paths.css))
    .pipe(rename({ suffix: '.min' }))
    .pipe(postcss(minifyCss)) // Minifies the result
    .pipe(dest(paths.css))
}



// Styles email
function stylesEmail() {
  var processCss = [
      autoprefixer(), // adds vendor prefixes
      pixrem(),       // add fallbacks for rem units
  ]

  var minifyCss = [
      cssnano({ preset: 'default' })   // minify result
  ]

  return src(`${paths.sass}/email.scss`)
    .pipe(sass({
      includePaths: paths.cssSource
    }).on('error', sass.logError))
    .pipe(plumber()) // Checks for errors
    .pipe(postcss(processCss))
    .pipe(dest(paths.css))
    .pipe(rename({ suffix: '.min' }))
    .pipe(postcss(minifyCss)) // Minifies the result
    .pipe(dest(paths.css))
}



// Javascript minification
function scripts() {
  return src(paths.projectJs)
    .pipe(sourcemaps.init()) //https://github.com/gulp-sourcemaps/gulp-sourcemaps
    .pipe(plumber()) // Checks for errors
    .pipe(concat('project.js'))
    // .pipe(sourcemaps.write()) //https://github.com/gulp-sourcemaps/gulp-sourcemaps
    // .pipe(dest(paths.js))
    .pipe(gulpif(process.env.NODE_ENV !== 'development',terser())) // Minifies the js
    .pipe(rename({ suffix: '.min' }))
    .pipe(sourcemaps.write()) //https://github.com/gulp-sourcemaps/gulp-sourcemaps
    .pipe(dest(paths.js))
}
// function scripts() {
//   return src(paths.projectJs)
//     .pipe(gulpif(process.env.NODE_ENV === 'development', sourcemaps.init())) //https://github.com/gulp-sourcemaps/gulp-sourcemaps
//     .pipe(plumber()) // Checks for errors
//     .pipe(concat('project.js'))
//     // .pipe(sourcemaps.write()) //https://github.com/gulp-sourcemaps/gulp-sourcemaps
//     // .pipe(dest(paths.js))
//     .pipe(gulpif(process.env.NODE_ENV !== 'development',terser())) // Minifies the js
//     .pipe(rename({ suffix: '.min' }))
//     .pipe(gulpif(process.env.NODE_ENV === 'development', sourcemaps.write())) //https://github.com/gulp-sourcemaps/gulp-sourcemaps
//     .pipe(dest(paths.js))
// }

// Vendor Javascript minification
function vendorScripts() {
  return src(paths.vendorsJs)
    .pipe(sourcemaps.init())
    .pipe(concat('vendors.js'))
    .pipe(dest(paths.js))
    .pipe(plumber()) // Checks for errors
    .pipe(terser()) // Minifies the js
    .pipe(rename({ suffix: '.min' }))
    .pipe(sourcemaps.write())
    .pipe(dest(paths.js))
}
// function vendorScripts() {
//   return src(paths.vendorsJs)
//     .pipe(concat('vendors.js'))
//     .pipe(dest(paths.js))
//     .pipe(plumber()) // Checks for errors
//     .pipe(terser()) // Minifies the js
//     .pipe(rename({ suffix: '.min' }))
//     .pipe(dest(paths.js))
// }

// Image compression
function imgCompression() {
  return src(`${paths.images}/*`)
    .pipe(imagemin()) // Compresses PNG, JPEG, GIF and SVG images
    .pipe(dest(paths.images))
}


// Copy Fonts and font-icons from the /src folders
function copyFonts() {
  return src(paths.fontsSource)
  .pipe(dest(paths.fonts));
}


// Run django server
function asyncRunServer() {
  var cmd = spawn('gunicorn', [
      'config.asgi', '-k', 'uvicorn.workers.UvicornWorker', '--reload'
      ], {stdio: 'inherit'}
  )
  cmd.on('close', function(code) {
    console.log('gunicorn exited with code ' + code)
  })
}

// Browser sync server for live reload
function initBrowserSync() {
    browserSync.init(
      [
        `${paths.css}/*.css`,
        `${paths.js}/*.js`,
        `${paths.templates}/*.html`,
      ], {
        // https://www.browsersync.io/docs/options/#option-proxy
        proxy:  {
          target: proxy_target,
          proxyReq: [
            function(proxyReq, req) {
              // Assign proxy "host" header same as current request at Browsersync server
              proxyReq.setHeader('Host', req.headers.host)
            }
          ]
        },
        // https://www.browsersync.io/docs/options/#option-open
        // Disable as it doesn't work from inside a container
        open: false
      }
    )
}


// Watch
function watchPaths() {
  watch(`${paths.sass}/*.scss`, styles)
  watch(`${paths.templates}/**/*.html`).on("change", reload)
  watch(`${paths.projectJs}`, scripts).on("change", scripts)
}

// Generate all assets
const generateAssets = parallel(
  styles,
  stylesEmail,
  copyFonts,
  scripts,
  //vendorScripts,
  imgCompression
)

// Set up dev environment
const dev = parallel(
  initBrowserSync,
  watchPaths
)

exports.default = series(generateAssets, dev)
exports["generate-assets"] = generateAssets
exports["dev"] = dev
