"""
Base settings to build other settings files upon.
"""
from pathlib import Path
import os
import environ

ROOT_DIR = Path(__file__).resolve(strict=True).parent.parent.parent
# {{cookiecutter.project_slug}}/
APPS_DIR = ROOT_DIR / "{{cookiecutter.project_slug}}"
env = environ.Env()

READ_DOT_ENV_FILE = env.bool("DJANGO_READ_DOT_ENV_FILE", default=False)
if READ_DOT_ENV_FILE:
    # OS environment variables take precedence over variables from .env
    env.read_env(str(ROOT_DIR / ".env"))

# GENERAL
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = env.bool("DJANGO_DEBUG", False)
# Local time zone. Choices are
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# though not all of them may be available with every OS.
# In Windows, this must be set to your system time zone.
TIME_ZONE = "UTC"
#TIME_ZONE = "Europe/Berlin"

# https://docs.djangoproject.com/en/dev/ref/settings/#language-code
LANGUAGE_CODE = "en-us"
# https://docs.djangoproject.com/en/dev/ref/settings/#site-id
SITE_ID = 1
# https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
USE_I18N = True
# https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = True
# https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = True
# https://docs.djangoproject.com/en/dev/ref/settings/#locale-paths
LOCALE_PATHS = [str(ROOT_DIR / "locale")]

# DATABASES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {"default": env.db("DATABASE_URL")}
DATABASES["default"]["ATOMIC_REQUESTS"] = True
DEFAULT_AUTO_FIELD='django.db.models.AutoField'

# URLS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#root-urlconf
ROOT_URLCONF = "config.urls"
# https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = "config.wsgi.application"

# APPS
# ------------------------------------------------------------------------------
DJANGO_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.sites",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    # "django.contrib.humanize", # Handy template tags
    "django.contrib.admin",
    "django.forms",
]
THIRD_PARTY_APPS = [
    #"dbsettings",
    #"crispy_forms",
    "allauth",
    "allauth.account",
    "allauth.socialaccount",
    "django_celery_beat",
    "rest_framework",
    "rest_framework.authtoken",
    "corsheaders",
    "djoser",
    "webpack_loader",
    
]

LOCAL_APPS = [
    "{{cookiecutter.project_slug}}.users.apps.UsersConfig",
    "{{cookiecutter.project_slug}}.apps.{{cookiecutter.project_slug}}Config",
    "{{cookiecutter.project_slug}}.webpack_bundle.apps.WebpackBundleConfig",
    # Your stuff: custom apps go here
]
# https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

# MIGRATIONS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#migration-modules
MIGRATION_MODULES = {"sites": "{{cookiecutter.project_slug}}.contrib.sites.migrations"}

# AUTHENTICATION
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#authentication-backends
AUTHENTICATION_BACKENDS = [
    "django.contrib.auth.backends.ModelBackend",
    "allauth.account.auth_backends.AuthenticationBackend",
]
# https://docs.djangoproject.com/en/dev/ref/settings/#auth-user-model
AUTH_USER_MODEL = "users.User"
# https://docs.djangoproject.com/en/dev/ref/settings/#login-redirect-url
LOGIN_REDIRECT_URL = "users:redirect"
# https://docs.djangoproject.com/en/dev/ref/settings/#login-url
#LOGIN_URL = "account_login"

# PASSWORDS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#password-hashers
PASSWORD_HASHERS = [
    # https://docs.djangoproject.com/en/dev/topics/auth/passwords/#using-argon2-with-django
    "django.contrib.auth.hashers.Argon2PasswordHasher",
    "django.contrib.auth.hashers.PBKDF2PasswordHasher",
    "django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher",
    "django.contrib.auth.hashers.BCryptSHA256PasswordHasher",
]
# https://docs.djangoproject.com/en/dev/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"
    },
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"},
]

# MIDDLEWARE
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#middleware
MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "corsheaders.middleware.CorsMiddleware",
    #"corsheaders.middleware.CorsPostCsrfMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.common.BrokenLinkEmailsMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",

]

# STATIC
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = str(ROOT_DIR / "staticfiles")
# https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL = "/static/"
# https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
# commented out, since the main app static resources is already found via AppDirectoriesFinder
# STATICFILES_DIRS = [str(APPS_DIR / "static")]

# https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#staticfiles-finders
STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
]

# MEDIA
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#media-root
MEDIA_ROOT = str(APPS_DIR / "media")
# https://docs.djangoproject.com/en/dev/ref/settings/#media-url
MEDIA_URL = "/media/"

# TEMPLATES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#templates
TEMPLATES = [
    {
        # https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-TEMPLATES-BACKEND
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        # https://docs.djangoproject.com/en/dev/ref/settings/#template-dirs
        "DIRS": [str(APPS_DIR / "templates")],
        "OPTIONS": {
            # https://docs.djangoproject.com/en/dev/ref/settings/#template-loaders
            # https://docs.djangoproject.com/en/dev/ref/templates/api/#loader-types
            "loaders": [
                "django.template.loaders.filesystem.Loader",
                "django.template.loaders.app_directories.Loader",
            ],
            # https://docs.djangoproject.com/en/dev/ref/settings/#template-context-processors
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.static",
                "django.template.context_processors.tz",
                "django.contrib.messages.context_processors.messages",
                "{{cookiecutter.project_slug}}.utils.context_processors.settings_context",
            ],
        },
    }
]

# https://docs.djangoproject.com/en/dev/ref/settings/#form-renderer
FORM_RENDERER = "django.forms.renderers.TemplatesSetting"

# http://django-crispy-forms.readthedocs.io/en/latest/install.html#template-packs
# = "bootstrap4"

# FIXTURES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#fixture-dirs
#FIXTURE_DIRS = (str(APPS_DIR / "fixtures"),)


# SECURITY
# False is the django default: store CSRF token not in user session but in csrf cookie
CSRF_USE_SESSIONS=False



# https://docs.djangoproject.com/en/dev/ref/settings/#csrf-cookie-httponly
# False means: no HttpOnly flag on the CSRF cookie, meaning Client-side JavaScript will be able to access the CSRF cookie
# meaningless if you are not using CSRF cookie via CSRF_USE_SESSIONS=True
CSRF_COOKIE_HTTPONLY = False


# Since the frontend is on `http://127.0.0.1:8080` and the backend is on `http://127.0.0.1:8000`, they are two different origins and we need to set samesite to false
# UPDATE: Starting with django 3.1 this must be set to 'None'
# see also https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie/SameSite
# Important: CORS_ALLOW_CREDENTIALS setting True implies setting this to "None"
CSRF_COOKIE_SAMESITE = 'None'

# A list of trusted origins for unsafe requests (e.g. POST).
CSRF_TRUSTED_ORIGINS = env.list("DJANGO_CSRF_TRUSTED_ORIGINS", default=["http://127.0.0.1", "https://127.0.0.1"])

# If this is set to True, the cookie will be marked as “secure”, which means browsers 
# may ensure that the cookie is only sent with an HTTPS connection.
CSRF_COOKIE_SECURE = True
#CSRF_COOKIE_SECURE = False


# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#session-cookie-httponly
# client-side JavaScript will  be able to access the SESSION cookie
SESSION_COOKIE_HTTPONLY = False

# see above for CSRF cookies
# Important: CORS_ALLOW_CREDENTIALS setting True implies setting this to "None"
SESSION_COOKIE_SAMESITE = 'None'

# If this is set to True, the cookie will be marked as “secure”, which means browsers 
# may ensure that the cookie is only sent with an HTTPS connection.
SESSION_COOKIE_SECURE = True


# CORS
# django-cors-headers - https://github.com/adamchainz/django-cors-headers#setup
# restrict to certain pages that are served by django for which CORS headers are sent
CORS_URLS_REGEX = r"^/api/.*$"

# cookies will be allowed to be included in cross-site HTTP requests
# IMPORTANT (tbc): this means CORS_ALLOW_ALL_ORIGINS = True cannot be set, but you have to list the allowed origins in CORS_ALLOWED_ORIGINS
#  (see here: https://blog.vnaik.com/posts/web-attacks.html )
CORS_ALLOW_CREDENTIALS = False


# https://docs.djangoproject.com/en/dev/ref/settings/#secure-browser-xss-filter
#SECURE_BROWSER_XSS_FILTER = True
# https://docs.djangoproject.com/en/dev/ref/settings/#x-frame-options
#X_FRAME_OPTIONS = "DENY"




# EMAIL
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
EMAIL_BACKEND = env(
    "DJANGO_EMAIL_BACKEND", default="django.core.mail.backends.smtp.EmailBackend"
)
# https://docs.djangoproject.com/en/dev/ref/settings/#email-timeout
EMAIL_TIMEOUT = 5

# ADMIN
# ------------------------------------------------------------------------------
# Django Admin URL.
ADMIN_URL = "admin/"
# https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS = [("""{{cookiecutter.author_name}}""", "{{cookiecutter.email}}")]
# https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS = ADMINS

# LOGGING
# ------------------------------------------------------------------------------
DJANGO_LOGFILE = env("DJANGO_LOGFILE", default="")
DJANGO_LOGLEVEL= env("DJANGO_LOGLEVEL", default = "WARNING")

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
    },
    'formatters': {
        'default': {
            # exact format is not important, this is the minimum informationstyle asctime
            'format': '[%(asctime)s] %(name)-12s %(levelname)-8s %(message)s',
            'datefmt': '%y-%m-%d, %H:%M:%S',
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
            'formatter': 'default',
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
    },
     'loggers': {
        # default for all undefined Python modules
        '': {
            'level': DJANGO_LOGLEVEL,
            'handlers': ['console', 'file' ] if DJANGO_LOGFILE else ['console'],
			 
        },
        'py.warnings': {
            'level': DJANGO_LOGLEVEL,
            'handlers': ['console', 'file'] if DJANGO_LOGFILE else ['console'],
        },
         'django': {
             'handlers': ['console', 'mail_admins', 'file'] if DJANGO_LOGFILE else ['console', 'mail_admins'] ,
						 #'handlers': ['console'],
             'level': DJANGO_LOGLEVEL,
             'propagate': False,
         },
        # Our application code
        '{{cookiecutter.project_slug}}': {
            'level': DJANGO_LOGLEVEL,
            'handlers': ['console', 'mail_admins', 'file'] if DJANGO_LOGFILE else ['console', 'mail_admins'],
            # Avoid double logging because of root logger
            'propagate': False,
        },
        # Note: logs from celery workers/beat themselves go into respective settings given celery app directly
        'celery': {
            'level': DJANGO_LOGLEVEL,
            'handlers': ['console', 'file'] if DJANGO_LOGFILE else ['console'],
            # Avoid double logging because of root logger
            'propagate': False,
        },
        # Prevent noisy modules from logging
        'matplotlib': {
            'level': 'ERROR',
            'handlers': ['console', 'file'] if DJANGO_LOGFILE else ['console'],
            'propagate': False,
        },
    },
}

if DJANGO_LOGFILE:
    LOGGING['handlers']['file'] = {
        'level': DJANGO_LOGLEVEL,
        'class': 'logging.handlers.RotatingFileHandler',
        'formatter': 'default',
        'filename': DJANGO_LOGFILE,
        'maxBytes': 1024 * 1024 * 10,  # 10 mb
        }



# Celery
# ------------------------------------------------------------------------------
if USE_TZ:
    # http://docs.celeryproject.org/en/latest/userguide/configuration.html#std:setting-timezone
    CELERY_TIMEZONE = TIME_ZONE
# http://docs.celeryproject.org/en/latest/userguide/configuration.html#std:setting-broker_url
CELERY_BROKER_URL = env("CELERY_BROKER_URL")
# http://docs.celeryproject.org/en/latest/userguide/configuration.html#std:setting-result_backend
CELERY_RESULT_BACKEND = CELERY_BROKER_URL
# http://docs.celeryproject.org/en/latest/userguide/configuration.html#std:setting-accept_content
CELERY_ACCEPT_CONTENT = ["json"]
# http://docs.celeryproject.org/en/latest/userguide/configuration.html#std:setting-task_serializer
CELERY_TASK_SERIALIZER = "json"
# http://docs.celeryproject.org/en/latest/userguide/configuration.html#std:setting-result_serializer
CELERY_RESULT_SERIALIZER = "json"
# http://docs.celeryproject.org/en/latest/userguide/configuration.html#task-time-limit
# TODO: set to whatever value is adequate in your circumstances
CELERY_TASK_TIME_LIMIT = 60 * 60
# http://docs.celeryproject.org/en/latest/userguide/configuration.html#task-soft-time-limit
# TODO: set to whatever value is adequate in your circumstances
#CELERY_TASK_SOFT_TIME_LIMIT = 60
# http://docs.celeryproject.org/en/latest/userguide/configuration.html#beat-scheduler
CELERY_BEAT_SCHEDULER = "django_celery_beat.schedulers:DatabaseScheduler"

# CELERY_TASK_DEFAULT_QUEUE = 'queue-everken-default-dev'

# CELERY_TASK_ROUTES = {
#     'enersysopt.tasks.optimize': {'queue': 'queue-everken-optimization-dev'},
#     'enersysopt.tasks.optimize_internal': {'queue': 'queue-everken-optimization-dev'},
#     'enersysopt.send_email': {'queue': 'queue-everken-default-dev'},
#     'enersysopt.make_plots': {'queue': 'queue-everken-default-dev'},
# }

# CELERY_BEAT_SCHEDULE = {
#     'cleanup_expired_requests': {
#         'task': 'enersysopt.tasks.cleanup_expired_requests',
#         'schedule': datetime.timedelta(hours=6),
#     },
#     #    'testbeam': {
#     #        'task': 'enersysopt.tasks.testperiodictask',
#     #        'schedule': datetime.timedelta(seconds=5),
#     #    },
# }





# django-allauth
# ------------------------------------------------------------------------------
ACCOUNT_ALLOW_REGISTRATION = env.bool("DJANGO_ACCOUNT_ALLOW_REGISTRATION", True)
ACCOUNT_SIGNUP_PASSWORD_ENTER_TWICE = False
# https://django-allauth.readthedocs.io/en/latest/configuration.html
ACCOUNT_AUTHENTICATION_METHOD = "email"
#ACCOUNT_SESSION_REMEMBER = True
# https://django-allauth.readthedocs.io/en/latest/configuration.html
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_UNIQUE_EMAIL = True
ACCOUNT_USERNAME_REQUIRED = False
# https://django-allauth.readthedocs.io/en/latest/configuration.html
ACCOUNT_EMAIL_VERIFICATION = "none"
# https://django-allauth.readthedocs.io/en/latest/configuration.html
ACCOUNT_ADAPTER = "{{cookiecutter.project_slug}}.users.adapters.AccountAdapter"
# https://django-allauth.readthedocs.io/en/latest/configuration.html
SOCIALACCOUNT_ADAPTER = "{{cookiecutter.project_slug}}.users.adapters.SocialAccountAdapter"

# django-rest-framework
# -------------------------------------------------------------------------------
# django-rest-framework - https://www.django-rest-framework.org/api-guide/settings/
REST_FRAMEWORK = {
    "DEFAULT_AUTHENTICATION_CLASSES": (
        #"rest_framework.authentication.SessionAuthentication",
        "rest_framework.authentication.TokenAuthentication",
    ),
    "DEFAULT_PERMISSION_CLASSES": ("rest_framework.permissions.IsAuthenticated",),
}



# Vue
# -------------------------------------------------------------------------------
VUE_FRONTEND_DIR = Path(ROOT_DIR, "vue_frontend")
WEBPACK_LOADER = {
    "DEFAULT": {
        "CACHE": not DEBUG,
        "BUNDLE_DIR_NAME": "vue/",  # must end with slash
        "STATS_FILE": Path(VUE_FRONTEND_DIR, "webpack-stats.json"),
        "POLL_INTERVAL": 1,
        "TIMEOUT": None,
        "IGNORE": [r".+\.hot-update.js", r".+\.map"],
    }
}

# Your stuff...
# ------------------------------------------------------------------------------
