from django.conf import settings
from django.urls import path, include

from rest_framework.routers import DefaultRouter, SimpleRouter

if settings.DEBUG:
   router = DefaultRouter()
else:
   router = SimpleRouter()

#router.register("users", UserViewSet)

app_name = "{{cookiecutter.project_slug}}"
urlpatterns = router.urls

