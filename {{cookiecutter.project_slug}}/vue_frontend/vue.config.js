const BundleTracker = require("webpack-bundle-tracker");
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;
const fs = require('fs')
const path = require('path');

const pages = {
    'main': {
        entry: './src/{{cookiecutter.project_slug}}/main.js',
        //chunks: ['chunk-common', 'chunk-state']
    },
}

module.exports = {
    pages: pages,
    filenameHashing: true,
    productionSourceMap: false,

    publicPath: process.env.NODE_ENV === 'production'
        ? '/static/vue'
        : 'http://127.0.0.1:8080/',

    outputDir: '../{{cookiecutter.project_slug}}/static/vue/',

    configureWebpack: {
        resolve: {
            //allow for @ or @src alias for src
            alias: require('./aliases.config').webpack
        },

        plugins: [
            new BundleTracker({
                path: '../vue_frontend/',
                filename: 'webpack-stats.json'
            }),
        ],

        //watch: true,
        watchOptions: {
            poll: 1000, // Check for changes every second
        },

        // usually defaults to web, but when a browserslistrc or related config in package.json is set, it is reset to browserslist - that breaks the dvmnt server
        target: process.env.NODE_ENV === 'production'
            ? 'browserslist'
            : 'web',

        output: {
            filename: "[name]-[fullhash].js",
            chunkFilename: "[name]-[fullhash].js"
        },


    },

    chainWebpack: config => {

        // config.optimization
        //     .splitChunks({
        //         cacheGroups: {
        //             // state: {
        //             //     /* As vuex state is not needed in all our entry points, we isolate it
        //             //      * in a separate chunk to be loaded only where needed.
        //             //      */
        //             //     test: /[\\/]node_modules[\\/](vuex|vuex-persisted-state)/,
        //             //     name: "chunk-state",
        //             //     chunks: "all",
        //             //     priority: 5
        //             // },
        //             // vendor: {
        //             //     /* This chunk contains modules that may be used in all entry points,
        //             //      * including Vue itself
        //             //      */
        //             //     test: /[\\/]node_modules[\\/]/,
        //             //     name: "chunk-common",
        //             //     chunks: "all",
        //             //     priority: 1
        //             // },
        //         },
        //     });

        Object.keys(pages).forEach(page => {
            config.plugins.delete(`html-${page}`);
            config.plugins.delete(`preload-${page}`);
            config.plugins.delete(`prefetch-${page}`);
        })

        config.module
            .rule('scss')
            .oneOf('vue')
            .use('resolve-url-loader')
            .loader('resolve-url-loader').options({
            })
            .before('sass-loader');

    },


    devServer: {
        hot: true,
        client: {
            webSocketURL: {
                hostname: "localhost",
                port: 8080,
            },
        },
        static: {
            watch: true,
        },
        historyApiFallback: true,
        headers: { 'Access-Control-Allow-Origin': '*' },

        // https: {
        //     ca: "./server.pem",
        //     pfx: "./server.pfx",
        //     key: "./server.key",
        //     cert: "./server.crt",
        //     passphrase: "webpack-dev-server",
        //     requestCert: true,
        //   },
    },


    pluginOptions: {
      i18n: {
        locale: 'en',
        fallbackLocale: 'en',
        localeDir: 'locales',
        enableInSFC: true,
        enableBridge: false
      }
    }
};   
    
