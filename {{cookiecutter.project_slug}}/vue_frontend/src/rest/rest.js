import axios from "axios";

// axios settings
axios.defaults.baseURL = process.env.VUE_APP_API_ROOT;
//axios.defaults.xsrfHeaderName = "X-CSRFToken";
//axios.defaults.xsrfCookieName = 'csrftoken';
//axios.defaults.headers['Content-Type'] = 'application/json';


const api = axios.create({});


export default {

    setAuthHeader(token) {
        api.defaults.headers.common['Authorization'] = 'Token ' + token
    },

    unsetAuthHeader() {
        api.defaults.headers.common['Authorization'] = ''
    },

    createUser(formdata) {
        return api.post("/users/", formdata)
    },

    getUserData() {
        return api.get("/users/me/")
    },

    login(formdata) {
        return api.post("/token/login/", formdata)
  
    },

    logout() {
        return api.post("/token/logout/")
    },

     /* Include additional API calls here */
}

