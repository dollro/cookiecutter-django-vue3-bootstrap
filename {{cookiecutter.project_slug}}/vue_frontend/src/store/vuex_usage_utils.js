import Vue from "vue/dist/vue.js";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";
import Cookies from "js-cookie";

Vue.use(Vuex);

let store = new Vuex.Store({
    modules: {},
    strict: process.env.NODE_ENV !== "production",
    plugins: [
        createPersistedState(
        //     {
        //     storage: {
        //         getItem: key => Cookies.get(key),
        //         setItem: (key, value) => Cookies.set(key, value, { expires: 3, secure: true }),
        //         removeItem: key => Cookies.remove(key)
        //     }
        // }
        )
    ]
});


function persistState(persistentPaths) {
    createPersistedState({
        paths: persistentPaths
    })(Vue.prototype.$store)
}


function persistState(persistentPaths) {
    createPersistedState({paths: persistentPaths})(Vue.prototype.$store)
}

export const VuexAsPlugin = {
    store,
    install(Vue) { //resetting the default store to use this plugin store
        Vue.prototype.$store = store;
    }
}

export function registerModules(modulesDef) {
    let persisted = [];
    for (let [mName, mDef] of Object.entries(modulesDef)) {
        if ('module' in mDef) {
            registerModule(mName, mDef['module']);
        }
        if ('persistedPaths' in mDef) {
            persisted = persisted.concat(mDef['persistedPaths'].map(p => mName + "." + p))
        }
    }
    persistState(persisted);
}
