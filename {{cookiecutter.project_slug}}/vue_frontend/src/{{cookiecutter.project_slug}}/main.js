//import Vue from "vue/dist/vue.js"
//import {VuexAsPlugin, registerModules} from "../store/vuex_usage_utils";

import StoreModule_{{cookiecutter.project_slug}} from "./store/store_{{cookiecutter.project_slug}}"
import {createAppInEl, createSharedStore} from "../utils/create_app_utils";
import MainApp from "./MainApp.vue"

// Mount top level components
let store = createSharedStore({
  main: StoreModule_{{cookiecutter.project_slug}},
});

createAppInEl(MainApp, store, "#vue-main-app");
