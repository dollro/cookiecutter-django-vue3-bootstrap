from django.contrib import admin

from {{cookiecutter.project_slug}}.models import TestModel


@admin.register(TestModel)
class TestModelAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', ]