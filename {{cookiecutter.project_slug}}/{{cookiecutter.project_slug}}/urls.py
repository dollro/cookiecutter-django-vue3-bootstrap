from django.urls import path
from django.views.generic import TemplateView
from . import views 

urlpatterns = [
    path("", TemplateView.as_view(template_name="home.html")),
    path("testmodel/", views.TestModelList.as_view()),
    path("testmodel/<int:pk>/", views.TestModelDetail.as_view()),
]
