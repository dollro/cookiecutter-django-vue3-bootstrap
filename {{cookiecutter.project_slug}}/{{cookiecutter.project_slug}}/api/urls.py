from django.urls import path
#from rest_framework.urlpatterns import format_suffix_patterns
from {{cookiecutter.project_slug}}.api import views as api_views


urlpatterns = [
    path("", api_views.TestModelList.as_view()),
    path("<int:pk>/", api_views.TestModelDetail.as_view()),
]