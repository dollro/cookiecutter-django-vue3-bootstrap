from rest_framework.response import Response
from {{cookiecutter.project_slug}}.models import TestModel

from .serializers import TestModelSerializer

from rest_framework.permissions import AllowAny, IsAuthenticated 
from rest_framework.decorators import authentication_classes, permission_classes

from rest_framework import generics, serializers, viewsets
from rest_framework import views as rest_views
from rest_framework.response import Response
from rest_framework import status

#@permission_classes((AllowAny, ))
class TestModelList(rest_views.APIView):
    def get(self, request, format=None):
        instances = TestModel.objects.all()
        serializer = TestModelSerializer(instances, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = TestModelSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class TestModelList2(generics.ListCreateAPIView):
    queryset = TestModel.objects.all()
    serializer_class = TestModelSerializer

class TestModelList3(generics.mixins.CreateModelMixin,
                   generics.mixins.RetrieveModelMixin,
                   generics.mixins.ListModelMixin,
                   viewsets.GenericViewSet):
    queryset = TestModel.objects.all()
    serializer_class = TestModelSerializer


class TestModelDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = TestModel.objects.all()
    serializer_class = TestModelSerializer







		