from rest_framework import serializers
from {{cookiecutter.project_slug}}.models import TestModel


class TestModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestModel
        fields = ["id", "name"]
