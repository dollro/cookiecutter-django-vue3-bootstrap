from django.views.generic import ListView, DetailView

from {{cookiecutter.project_slug}}.models import TestModel

class TestModelList(ListView):
    model = TestModel
    template_name = "testmodel_list.html"
class TestModelDetail(DetailView):
    model = TestModel
    template_name = "testmodel_detail.html"