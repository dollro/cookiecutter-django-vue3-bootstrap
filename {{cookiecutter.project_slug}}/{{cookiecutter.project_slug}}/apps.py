from django.apps import AppConfig


class {{cookiecutter.project_slug}}Config(AppConfig):
    name = "{{cookiecutter.project_slug}}"